Codage de Huffman
=================

Évaluation - rendre votre travail
---------------------------------

Le travail réalisé dans le cadre de ce TP sera pris en compte dans l'évaluation.

Ce travail sera rendu via un dépôt GitLab ad hoc. Voyez les instructions à

* [gitlab-fil.univ-lille.fr/diu-eil-lil/huffman](https://gitlab-fil.univ-lille.fr/diu-eil-lil/huffman)

Sujet de travaux pratiques
--------------------------

Le sujet rédigé dans un notebook jupyter est accessible

* dans le fichier [tp_codage_huffman.ipynb](tp_codage_huffman.ipynb) via le serveur `jupyter.fil.univ-lille1.fr` à [frama.link/diu-ipynb-huffman](http://frama.link/diu-ipynb-huffman)
* dans un fichier source Python généré depuis ce notebook [tp_codage_huffman.py](tp_codage_huffman.py)

Vous travaillez à votre préférence dans un notebook ou Thonny.

Les fichiers annexes suivants sont également fournis :

* [binary_tree.py](binary_tree.py)
* [cigale-UTF-8.txt](cigale-UTF-8.txt)
* [exple_huffman_tree.png](exple_huffman_tree.png)

