Calendrier 
==========

[DIU Enseigner l'informatique au lycée](./Readme.md)


* [Calendrier blocs 1 à 3, 2018/19](calendrier-b1-3-1819.md) pour mémoire


mardi 7 au jeudi 9 janvier 2020
===============================

* mercredi 8 janvier, 11h00-12h30, M5-Bacchus
* jeudi 9 janvier, 11h00-12h30, M5-Bacchus 

jeudi 19 décembre 2019
======================

[Cours compression](/bloc4-5/Readme.md#jeudi-19-d%C3%A9cembre-2019),
[TD et TP codage de Huffman](/bloc4-5/Readme.md#jeudi-19-d%C3%A9cembre-2019)


| quand       | quoi  | qui      | où         | avec qui |
|-------------|-------|----------|------------|----------|
| 9h00-10h00  | cours | tous     | M5-Bacchus | Mikaël |
|             |       |          |             |          |
| 10h15-12h00 | TD/TP | groupe 1 | M5-A11      | Mikaël, Philippe |
|        et   |       | groupe 2 | M5-A12      | Benoit, Jean-Stéphane |
| 13h15-16h30 |       | groupe 3 | M5-A14      | Éric, Francesco |

mercredi 18 décembre 2019
=========================

* accueil café bâtiment M5

[Suite et fin du cours de COO](/bloc4-5/Readme.md#mercredi-18-d%C3%A9cembre-2019),
[TD et TP _Arbres binaires et arbres de recherche_](/bloc4-5/Readme.md#mercredi-18-d%C3%A9cembre-2019)


| quand       | quoi  | qui      | où         | avec qui |
|-------------|-------|----------|------------|----------|
| 9h00-10h00  | cours | tous     | M5-Bacchus | Jean-Christophe |
|             |       |          |             |          |
| 10h15-12h00 | TD/TP | groupe 1 | M5-A11      | Benoit, Philippe |
|        et   |       | groupe 2 | M5-A12      | Éric, Jean-Stéphane |
| 13h15-16h30 |       | groupe 3 | M5-A14      | Jean-Christophe , Francesco |


mardi 17 décembre 2019
======================

_Formation reportée_.

lundi 2 décembre 2019
=====================

Accueil café au 1er étage extension bâtiment M3.


matin : [SQL - conception](bloc4-5/Readme.md#lundi-2-d%C3%A9cembre-2019)
-----------

| quand       | quoi  | qui      | où          | avec qui |
|-------------|-------|----------|-------------|----------|
| 9h00-10h20  | cours | tous     | M1-Painlevé | Patricia |
|             |       |          |             |          |
| 10h35-12h05 | TD/TP | groupe 1 | M5-A12      | Patricia |
|             |       | groupe 2 | M5-A15      | Benoit   |
|             |       | groupe 3 | M5-A4       | Bruno    |

après-midi : [cours POO](bloc4-5/Readme.md#lundi-2-d%C3%A9cembre-2019), [TD arbres binaires](bloc4-5/Readme.md#lundi-2-d%C3%A9cembre-2019)
---------------------------------

| quand       | quoi      | qui      | où         | avec qui        |
|-------------|-----------|----------|------------|-----------------|
| 13h15-14h45 | Cours COO | tous     | M1-Gallois | Jean-Christophe |
|             |           |          |            |                 |
| 15h00-16h30 | TD        | groupe 1 | M1-Riemann | Philippe        |
|             |           | groupe 2 | M1-Lie     | Benoit          |
|             |           | groupe 3 | SUP-08     | Jean-Christophe |


lundi 18 novembre 2019
======================

matin : [TP SQL en Python](bloc4-5/Readme.md#lundi-18-novembre-2019), [cours structures de données](bloc4-5/Readme.md#lundi-18-novembre-2019)
------------------------------------------

| quand       | quoi  | qui      | où                | avec qui |
|-------------|-------|----------|-------------------|----------|
| 9h00-11h00  | TP    | groupe 1 | A12               | Patricia |
|             |       | groupe 2 | A15               | Benoit   |
|             |       | groupe 3 | A4                | Maude    |
|             |       |          |                   |          |
| 11h15-12h15 | cours | tous     | P1 - Amphi Bruhat | Mikaël   |

après-midi : [Pile et File](bloc4-5/Readme.md#lundi-18-novembre-2019)
-------------------------

| quand       | quoi            | qui      | où                  | avec qui        |
|-------------|-----------------|----------|---------------------|-----------------|
| 13h15-15h15 | TP              | groupe 1 | M5-A12              | Éric            |
|             |                 | groupe 2 | M5-A15              | Jean-Stéphane   |
|             |                 | groupe 3 | M5-A4               | Jean-Christophe |
|             |                 |          |                     |                 |
| 15h30-16h30 | Temps d'échange | groupe 1 | M3-Salle du conseil | Éric            |
|             |                 | groupe 2 | M3-226              | Jean-Stéphane   |
|             |                 | groupe 3 | M3-Delattre         | Jean-Christophe |

lundi 4 novembre 2019
=====================

C'est la rentrée 2019/20 ! 

matin : [Découverte de SQL](bloc4-5/Readme.md#lundi-4-novembre-2019)
--------------------------------------------------------------------

| quand       | quoi  | qui      | où                | avec qui       |
|-------------|-------|----------|-------------------|----------------|
| 9h00-11h00  | TP    | groupe 1 | A12               | Patricia       |
|             |       | groupe 2 | A15               | Benoit         |
|             |       | groupe 3 | A4                | Maude          |
|             |       |          |                   | (+Éric à 10h)  |
| 11h15-12h15 | cours | tous     | P1 - Amphi Bruhat | Maude          |

après-midi : [Structure de données liste](bloc4-5/Readme.md#lundi-4-novembre-2019)
----------------------------------------------------------------------------------

| quand       | quoi | qui                           | où      | avec qui        |
|-------------|------|-------------------------------|---------|-----------------|
| 13h15-16h30 | TP   | groupe 1                      | M5-A4   | Philippe        |
|             |      | groupe 2 : BEAU à OBLED       | SUP-115 | Jean-Stéphane   |
|             |      | groupe 2 : SAUVAGE à ZOUAOUI  | SUP-116 | Jean-Christophe |
|             |      | groupe 3 : BROQUET à DEVERNAY | SUP-116 |Jean-Christophe  |
|             |      | groupe 3 : DUMETZ à VICOGNE   | SUP-117 | Éric            |



